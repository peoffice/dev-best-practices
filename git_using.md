#### Git配置
* 更改为你的用户名和邮箱
  * ```git config --global user.name "yourname"```
  * ```git config --global user.email "yourname@domain.com"```
* 配置仅推送当前分支
  * ```git config --global push.default simple```
* 配置禁用自动换行符替换（仅Windows）
  * ```git config --global core.autocrlf false```
* 配置gui文本编码（仅Windows）
  * ```git config --global gui.encoding utf-8```
* 查看配置
  * ```cat ~/.gitconfig```
* 代理配置
  * HTTP代理
    * ```git config --global http.https://domain.com.proxy http://proxyUsername:proxyPassword@proxy.server.com:port```
  * SSH代理
    * ```vim ~/ .ssh/config```
    * 添加内容
      > ```Host git.openearth.community ProxyCommand connect -H 10.192.124.220:80 %h %p ServerAliveInterval 30```

#### Git常用命令
* 说明
  * ```master: #默认开发分支```
  * ```origin: #默认远程版本库```
  * ```HEAD: #默认开发分支```
  * ```HEAD^: #Head的付提交```
* 创建版本库
  * ```git clone <url> #克隆远程版本库```
  * ```git init #初始化本地版本库``` 
* 修改和提交
  * ```git status #查看状态```
  * ```git diff #查看变更内容```
  * ```git add . #跟踪所有修改过的文件```
  * ```git add <file> #跟踪指定的文件```
  * ```git mv <old> <new> #文件改名```
  * ```git rm <file> #删除文件```
  * ```git rm --cached <file> #停止跟踪文件但不删除```
  * ```git commit -m "commit message" #提交所有更新过的文件```
  * ```git commit --amend #修改最后一次提交```
* 查看提交历史
  * ```git log #查看提交历史```
  * ```git log -p <file> #查看指定文件的提交历史```
  * ```git blame <file> #以列表方式查看指定文件的提交历史``` 
* 撤销
  * ```git reset --hard HEAD #撤销工作目录中所有未提交文件```
  * ```git checkout HEAD <file> #撤销指定的未提交文件的修改内容```
  * ```git revert <commit> #撤销指定的提交```
* 分支与标签
  * ```git branch #显示所有本地分支```
  * ```git checkout <branch/tag> #切换到指定分支或标签```
  * ```git branch <new-branch> #创建新分支```
  * ```git branch -d <branch> #删除本地分支```
  * ```git tag #列出所有本地标签```
  * ```git tag <tagname> #基于最新提交创建标签```
  * ```git tag -d <tagname> #删除标签```
* 合并与衍合
  * ```git merge <branch> #合并指定分支到当前分支```
  * ```git rebase <branch> #衍合指定分支到当前分支```
* 远程操作
  * ```git remote -v #查看远程版本库信息```
  * ```git remote show <remote> #查看指定远程版本库信息```
  * ```git remote add <remote> <url> #添加远程版本库```
  * ```git fetch <remote> #从远程库获取代码```
  * ```git pull <remote> <branch> #下载代码及快速合并```
  * ```git push <remote> <branch> #上传代码及快速合并```
  * ```git push <remote> :<branch/tag-name> #删除远程分支或标签```
  * ```git push --tags #上传所有标签```